variable "environment_tag" {
  type        = string
  description = "Environment tag value"
  default = "dev"
}
variable "azurerg" {
  type        = string
  description = "resource group 1"
  default = "RG04"
}
variable "location" {
  description = "The location for this Lab environment"
  type        = string
  default = "west us"
}
